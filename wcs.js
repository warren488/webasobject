module.exports = {
    functions: {
        reAuth: {
            args: ["uid", "email"],
            method: "POST",
            uri: "/api/adminlogin/",
            body: {
                "uid": "{{{uid}}}",
                "email": "{{{email}}}"
            },
            headers: {
                "Content-Type": "application/json"
            }
        },
        addProject: {
            args: ["project", "uid", "authorization"],
            method: "POST",
            uri: "/api/wcs/admin/projects/",
            body: "{{{project}}}",
            headers: {
                "Content-Type": "application/json",
                "uid": '{{{:creds:uid}}}',
                "authorization":"{{{:creds:authorization}}}"
            }
        }
    },
    authInfo: {
        authed: true,
        authPoint: {
            uri: "/api/adminlogin",
            method: "POST",
            requiredData: ["uid", "email"]
        },
        authData: {
            headers: ["uid", "Authorization"]
        }
    },
    endpoints: {
        about: {
            type: "collection",
            route: "/api/wcs/about/",
            authed: false,
        },
        projects: {
            type: "collection",
            route: "/api/wcs/admin/projects/",
            authed: true
        }
    },
    host: "localhost:3200"
}