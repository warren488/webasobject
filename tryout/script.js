// var host = 'https://api.foursquare.com/v2'
// var client_id = 'VPA3LUEDIPJ5A2RTRIZTV2P0XJKGCMFGWGL2DMSF1MZOKPE4'
// var client_secret = 'VPA3LUEDIPJ5A2RTRIZTV2P0XJKGCMFGWGL2DMSF1MZOKPE4'
// var uri = '/venues/search'

// var ll = '41.878114%2C%20-87.629798'
// var intent = 'checkin'
// var radius = 350
// var limit = 15
// var version = '20161016'

// var xhr = new XMLHttpRequest();

// var url = host + uri
// url += '?client_id=' + client_id
// url += '&client_secret=' + client_secret
// url += '&ll=' + ll
// url += '&intent=' + intent
// url += '&radius=' + radius
// url += '&limit=' + limit
// url += '&v=' + version

// xhr.open("get", url, true)

// xhr.onreadystatechange = function () {
//     if (this.readyState == 4 && this.status == 200) {
//         console.log((this.responseText))
//     } else if (this.readyState == 4 && this.status > 399 && this.status < 600) {
//         console.log(this.status)
//         console.log((this.responseText))
//     } else if (this.readyState == 4) {
//         console.log(this.status)
//         console.log((this.responseText))
//     }
// }

// // xhr.setRequestHeader("Content-Type", "Application/JSON")

// xhr.send()





// // var fsquare = require('../foresquare')
// var fsquare = {
//     host: "https://api.foursquare.com/v2",
//     local: {
//         client_id: 'VPA3LUEDIPJ5A2RTRIZTV2P0XJKGCMFGWGL2DMSF1MZOKPE4',
//         client_secret: 'G32RXWSJ5KFLUVV5TWA1KIVXGMNXDEMGAIMBWAPYOSY4VRKP',
//         version: '20161016'
//     },
//     functions: {
//         getVenue: {
//             args: ["venue_id"],
//             uri: "/venues/{{{venue_id}}}?v=20161016&client_id={{{:local:client_id}}}&client_secret={{{:local:client_secret}}}",
//             method: "GET",
//         },
//         searchForVenues: {
//             uri: "/venues/search?client_id={{{:local:client_id}}}&v=20161016&client_secret={{{:local:client_secret}}}",
//             method: "GET",
//             qparams: ['ll', 'near', 'intent', 'radius', 'sw', 'ne', 'query', 'limit', 'categoryId', 'llAcc', 'alt', 'altAcc', 'url', 'providerId', 'linkedId']

//         },
        
//     }

// }
// var foresquareAPI = new WAO({ init: fsquare })
// foresquareAPI.searchForVenues({
//     queryparams: {
//         ll: '41.878114%2C%20-87.629798',
//         intent: 'checkin',
//         radius: 350,
//         limit: 15
//     }
// }).then(console.log).catch(console.log)

















var init = {
    "openapi" : "3.0.0",
    "servers" : [ {
      "description" : "SwaggerHub API Auto Mocking",
      "url" : "https://us-central1-warrencs-fa828.cloudfunctions.net/petStore"
    } ],
    "info" : {
      "description" : "This is a sample Petstore server.  You can find\nout more about Swagger at\n[http://swagger.io](http://swagger.io) or on\n[irc.freenode.net, #swagger](http://swagger.io/irc/).\n",
      "version" : "1.1.0",
      "title" : "Swagger Petstore",
      "termsOfService" : "http://swagger.io/terms/",
      "contact" : {
        "email" : "apiteam@swagger.io"
      },
      "license" : {
        "name" : "Apache 2.0",
        "url" : "http://www.apache.org/licenses/LICENSE-2.0.html"
      }
    },
    "tags" : [ {
      "name" : "pet",
      "description" : "Everything about your Pets",
      "externalDocs" : {
        "description" : "Find out more",
        "url" : "http://swagger.io"
      }
    } ],
    "paths" : {
      "/people" : {
        "get" : {
          "tags" : [ "people" ],
          "summary" : "Get all people in the database",
          "operationId" : "getAllPeople",
          "parameters" : [ {
            "in" : "query",
            "description" : "query parameter used to limit the number of people objects returned",
            "name" : "limit",
            "schema" : {
              "type" : "integer",
              "example" : 3
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "name of user",
            "name" : "name",
            "schema" : {
              "type" : "string",
              "example" : "warren"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "password of user",
            "name" : "password",
            "schema" : {
              "type" : "string",
              "example" : "password"
            }
          } ],
          "responses" : {
            "400" : {
              "description" : "Bad Request"
            },
            "200" : {
              "description" : "Successful operation",
              "content" : {
                "application/json" : {
                  "schema" : {
                    "type" : "object",
                    "properties" : {
                      "personId" : {
                        "$ref" : "#/components/schemas/Person"
                      }
                    }
                  }
                }
              }
            }
          }
        },
        "post" : {
          "tags" : [ "people" ],
          "summary" : "Add new person to the database",
          "operationId" : "addPlayer",
          "responses" : {
            "400" : {
              "description" : "Bad Request"
            },
            "201" : {
              "description" : "Successful operation",
              "content" : {
                "application/json" : {
                  "schema" : {
                    "type" : "object",
                    "properties" : {
                      "status" : {
                        "type" : "integer"
                      },
                      "message" : {
                        "type" : "string"
                      }
                    },
                    "example" : {
                      "status" : 201,
                      "message" : "Person successfully created"
                    }
                  }
                }
              }
            }
          },
          "requestBody" : {
            "content" : {
              "application/json" : {
                "schema" : {
                  "$ref" : "#/components/schemas/Person"
                }
              }
            }
          },
          "parameters" : [ {
            "in" : "header",
            "required" : true,
            "description" : "name of user",
            "name" : "name",
            "schema" : {
              "type" : "string",
              "example" : "warren"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "password of user",
            "name" : "password",
            "schema" : {
              "type" : "string",
              "example" : "password"
            }
          } ]
        }
      },
      "/people/{personId}" : {
        "get" : {
          "tags" : [ "people" ],
          "summary" : "Get a specific person",
          "operationId" : "getPerson",
          "responses" : {
            "400" : {
              "description" : "Bad Request"
            },
            "200" : {
              "description" : "Successful operation",
              "content" : {
                "application/json" : {
                  "schema" : {
                    "$ref" : "#/components/schemas/Person"
                  }
                }
              }
            }
          },
          "parameters" : [ {
            "name" : "personId",
            "in" : "path",
            "required" : true,
            "schema" : {
              "type" : "string"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "name of user",
            "name" : "name",
            "schema" : {
              "type" : "string",
              "example" : "warren"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "password of user",
            "name" : "password",
            "schema" : {
              "type" : "string",
              "example" : "password"
            }
          } ]
        },
        "delete" : {
          "tags" : [ "people" ],
          "summary" : "Get a specific person",
          "operationId" : "getPersonr",
          "responses" : {
            "400" : {
              "description" : "Bad Request"
            },
            "200" : {
              "description" : "Successfully deleted",
              "content" : {
                "application/json" : {
                  "schema" : {
                    "type" : "object",
                    "properties" : {
                      "status" : {
                        "type" : "integer"
                      },
                      "message" : {
                        "type" : "string"
                      }
                    },
                    "example" : {
                      "status" : 200,
                      "message" : "successfully deleted"
                    }
                  }
                }
              }
            }
          },
          "parameters" : [ {
            "name" : "personId",
            "in" : "path",
            "required" : true,
            "schema" : {
              "type" : "string"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "name of user",
            "name" : "name",
            "schema" : {
              "type" : "string",
              "example" : "warren"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "password of user",
            "name" : "password",
            "schema" : {
              "type" : "string",
              "example" : "password"
            }
          } ]
        },
        "post" : {
          "tags" : [ "people" ],
          "summary" : "Update a person in the database changing ONLY the data specified",
          "operationId" : "nonDestructiveUpdatePlayer",
          "responses" : {
            "400" : {
              "description" : "Bad Request"
            },
            "201" : {
              "description" : "Successful operation",
              "content" : {
                "application/json" : {
                  "schema" : {
                    "type" : "object",
                    "properties" : {
                      "status" : {
                        "type" : "integer"
                      },
                      "message" : {
                        "type" : "string"
                      }
                    },
                    "example" : {
                      "status" : 201,
                      "message" : "Person successfully created"
                    }
                  }
                }
              }
            }
          },
          "requestBody" : {
            "content" : {
              "application/json" : {
                "schema" : {
                  "$ref" : "#/components/schemas/Person"
                }
              }
            }
          },
          "parameters" : [ {
            "name" : "personId",
            "description" : "The ID of the person to be updated",
            "in" : "path",
            "required" : true,
            "schema" : {
              "type" : "string"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "name of user",
            "name" : "name",
            "schema" : {
              "type" : "string",
              "example" : "warren"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "password of user",
            "name" : "password",
            "schema" : {
              "type" : "string",
              "example" : "password"
            }
          } ]
        },
        "put" : {
          "tags" : [ "people" ],
          "summary" : "Update a person in the database removing all unspecified data",
          "operationId" : "destructiveUpdatePlayer",
          "responses" : {
            "400" : {
              "description" : "Bad Request"
            },
            "201" : {
              "description" : "Successful operation",
              "content" : {
                "application/json" : {
                  "schema" : {
                    "type" : "object",
                    "properties" : {
                      "status" : {
                        "type" : "integer"
                      },
                      "message" : {
                        "type" : "string"
                      }
                    },
                    "example" : {
                      "status" : 201,
                      "message" : "Person successfully created"
                    }
                  }
                }
              }
            }
          },
          "requestBody" : {
            "content" : {
              "application/json" : {
                "schema" : {
                  "$ref" : "#/components/schemas/Person"
                }
              }
            }
          },
          "parameters" : [ {
            "name" : "personId",
            "description" : "The ID of the person to be updated",
            "in" : "path",
            "required" : true,
            "schema" : {
              "type" : "string"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "name of user",
            "name" : "name",
            "schema" : {
              "type" : "string",
              "example" : "warren"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "password of user",
            "name" : "password",
            "schema" : {
              "type" : "string",
              "example" : "password"
            }
          } ]
        }
      },
      "/pet" : {
        "get" : {
          "tags" : [ "pet" ],
          "summary" : "Get all pets in the database",
          "operationId" : "getAllPeople",
          "parameters" : [ {
            "in" : "query",
            "description" : "query parameter used to limit the number of pet objects returned",
            "name" : "limit",
            "schema" : {
              "type" : "integer",
              "example" : 3
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "name of user",
            "name" : "name",
            "schema" : {
              "type" : "string",
              "example" : "warren"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "password of user",
            "name" : "password",
            "schema" : {
              "type" : "string",
              "example" : "password"
            }
          } ],
          "responses" : {
            "200" : {
              "description" : "successful response",
              "content" : {
                "application/json" : {
                  "schema" : {
                    "type" : "object",
                    "properties" : {
                      "petId" : {
                        "$ref" : "#/components/schemas/Pet"
                      }
                    }
                  }
                }
              }
            }
          }
        },
        "post" : {
          "tags" : [ "pet" ],
          "summary" : "Add a new pet to the store",
          "operationId" : "addPet",
          "responses" : {
            "405" : {
              "description" : "Invalid input"
            }
          },
          "requestBody" : {
            "$ref" : "#/components/requestBodies/Pet"
          },
          "parameters" : [ {
            "in" : "header",
            "required" : true,
            "description" : "name of user",
            "name" : "name",
            "schema" : {
              "type" : "string",
              "example" : "warren"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "password of user",
            "name" : "password",
            "schema" : {
              "type" : "string",
              "example" : "password"
            }
          } ]
        }
      },
      "/pet/findByStatus" : {
        "get" : {
          "tags" : [ "pet" ],
          "summary" : "Finds Pets by status",
          "description" : "Multiple status values can be provided with comma separated strings",
          "operationId" : "findPetsByStatus",
          "parameters" : [ {
            "name" : "status",
            "in" : "query",
            "description" : "Status values that need to be considered for filter",
            "required" : true,
            "explode" : true,
            "schema" : {
              "type" : "array",
              "items" : {
                "type" : "string",
                "enum" : [ "available", "pending", "sold" ],
                "default" : "available"
              }
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "name of user",
            "name" : "name",
            "schema" : {
              "type" : "string",
              "example" : "warren"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "password of user",
            "name" : "password",
            "schema" : {
              "type" : "string",
              "example" : "password"
            }
          } ],
          "responses" : {
            "200" : {
              "description" : "successful operation",
              "content" : {
                "application/json" : {
                  "schema" : {
                    "type" : "array",
                    "items" : {
                      "$ref" : "#/components/schemas/Pet"
                    }
                  }
                },
                "application/xml" : {
                  "schema" : {
                    "type" : "array",
                    "items" : {
                      "$ref" : "#/components/schemas/Pet"
                    }
                  }
                }
              }
            },
            "400" : {
              "description" : "Invalid status value"
            }
          }
        }
      },
      "/pet/{petId}" : {
        "get" : {
          "tags" : [ "pet" ],
          "summary" : "Find pet by ID",
          "description" : "Returns a single pet",
          "operationId" : "getPetById",
          "parameters" : [ {
            "name" : "petId",
            "in" : "path",
            "description" : "ID of pet to return",
            "required" : true,
            "schema" : {
              "type" : "integer",
              "format" : "int64"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "name of user",
            "name" : "name",
            "schema" : {
              "type" : "string",
              "example" : "warren"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "password of user",
            "name" : "password",
            "schema" : {
              "type" : "string",
              "example" : "password"
            }
          } ],
          "responses" : {
            "200" : {
              "description" : "successful operation",
              "content" : {
                "application/json" : {
                  "schema" : {
                    "$ref" : "#/components/schemas/Pet"
                  }
                },
                "application/xml" : {
                  "schema" : {
                    "$ref" : "#/components/schemas/Pet"
                  }
                }
              }
            },
            "400" : {
              "description" : "Invalid ID supplied"
            },
            "404" : {
              "description" : "Pet not found"
            }
          }
        },
        "post" : {
          "tags" : [ "pet" ],
          "summary" : "Updates a pet in the store replacing ONLY specified data",
          "operationId" : "nonDestructiveUpdate",
          "parameters" : [ {
            "name" : "petId",
            "in" : "path",
            "description" : "ID of pet that needs to be updated",
            "required" : true,
            "schema" : {
              "type" : "integer",
              "format" : "int64"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "name of user",
            "name" : "name",
            "schema" : {
              "type" : "string",
              "example" : "warren"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "password of user",
            "name" : "password",
            "schema" : {
              "type" : "string",
              "example" : "password"
            }
          } ],
          "responses" : {
            "405" : {
              "description" : "Invalid input"
            }
          },
          "requestBody" : {
            "content" : {
              "application/json" : {
                "schema" : {
                  "$ref" : "#/components/schemas/Pet"
                }
              }
            }
          }
        },
        "put" : {
          "tags" : [ "pet" ],
          "summary" : "Updates a pet in the store deleting all unspecified data",
          "operationId" : "destructive update",
          "parameters" : [ {
            "name" : "petId",
            "in" : "path",
            "description" : "ID of pet that needs to be updated",
            "required" : true,
            "schema" : {
              "type" : "integer",
              "format" : "int64"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "name of user",
            "name" : "name",
            "schema" : {
              "type" : "string",
              "example" : "warren"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "password of user",
            "name" : "password",
            "schema" : {
              "type" : "string",
              "example" : "password"
            }
          } ],
          "responses" : {
            "405" : {
              "description" : "Invalid input"
            }
          },
          "requestBody" : {
            "content" : {
              "application/json" : {
                "schema" : {
                  "$ref" : "#/components/schemas/Pet"
                }
              }
            }
          }
        },
        "delete" : {
          "tags" : [ "pet" ],
          "summary" : "Deletes a pet",
          "operationId" : "deletePet",
          "parameters" : [ {
            "name" : "api_key",
            "in" : "header",
            "required" : false,
            "schema" : {
              "type" : "string"
            }
          }, {
            "name" : "petId",
            "in" : "path",
            "description" : "Pet id to delete",
            "required" : true,
            "schema" : {
              "type" : "integer",
              "format" : "int64"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "name of user",
            "name" : "name",
            "schema" : {
              "type" : "string",
              "example" : "warren"
            }
          }, {
            "in" : "header",
            "required" : true,
            "description" : "password of user",
            "name" : "password",
            "schema" : {
              "type" : "string",
              "example" : "password"
            }
          } ],
          "responses" : {
            "400" : {
              "description" : "Invalid ID supplied"
            },
            "404" : {
              "description" : "Pet not found"
            }
          }
        }
      }
    },
    "externalDocs" : {
      "description" : "Find out more about Swagger",
      "url" : "http://swagger.io"
    },
    "components" : {
      "schemas" : {
        "Person" : {
          "type" : "object",
          "required" : [ "firstName", "lastName", "age", "pets" ],
          "properties" : {
            "firstName" : {
              "type" : "string"
            },
            "lastName" : {
              "type" : "string"
            },
            "address" : {
              "type" : "string"
            },
            "country" : {
              "type" : "string"
            },
            "age" : {
              "type" : "integer"
            },
            "pets" : {
              "type" : "array",
              "items" : {
                "type" : "integer"
              }
            }
          },
          "example" : {
            "firstName" : "Warren",
            "lastName" : "Scantlebury",
            "address" : "lot #1 Green Hill",
            "country" : "Barbados",
            "role" : "bowler",
            "age" : 22,
            "pets" : [ 2, 44, 9 ]
          }
        },
        "Pet" : {
          "type" : "object",
          "required" : [ "name", "ownerId", "status" ],
          "properties" : {
            "id" : {
              "type" : "integer",
              "format" : "int64"
            },
            "name" : {
              "type" : "string",
              "example" : "doggie"
            },
            "ownerId" : {
              "type" : "string",
              "example" : "BUHG&&V&89g898989gh9888h0"
            },
            "previousOwners" : {
              "type" : "array",
              "items" : {
                "type" : "integer"
              },
              "example" : [ 3, 55 ]
            },
            "photoUrls" : {
              "type" : "array",
              "xml" : {
                "name" : "photoUrl",
                "wrapped" : true
              },
              "items" : {
                "type" : "string"
              }
            },
            "status" : {
              "type" : "string",
              "description" : "pet status in the store",
              "enum" : [ "available", "pending", "sold" ]
            }
          },
          "xml" : {
            "name" : "Pet"
          }
        }
      },
      "requestBodies" : {
        "Pet" : {
          "content" : {
            "application/json" : {
              "schema" : {
                "$ref" : "#/components/schemas/Pet"
              }
            },
            "application/xml" : {
              "schema" : {
                "$ref" : "#/components/schemas/Pet"
              }
            }
          },
          "description" : "Pet object that needs to be added to the store",
          "required" : true
        }
      },
      "securitySchemes" : {
        "petstore_auth" : {
          "type" : "oauth2",
          "flows" : {
            "implicit" : {
              "authorizationUrl" : "http://petstore.swagger.io/oauth/dialog",
              "scopes" : {
                "write:pets" : "modify pets in your account",
                "read:pets" : "read your pets"
              }
            }
          }
        },
        "api_key" : {
          "type" : "apiKey",
          "name" : "api_key",
          "in" : "header"
        }
      }
    }
  }



  var oasapi = new WAO({ init, spec: "oas3.0" })
  var postPet = oasapi.pet.post(
    {
        body: {
            ownerId: 'fb723w9hgevg&G7goGO7',
            name: "2",
            status: "available"
        }, headers: {
            name: "warren",
            password: "mypassw"
        }
    }
).then(console.log).catch(console.log)

oasapi.pet.get({
    headers: {
        name: "warren",
        password: "mypassw"
    }
}).then(console.log)

console.log(oasapi.pet)
// .get().then(console.log)