var cors = require("cors");
const express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.use(cors())
app.use(bodyParser.json());

app.use((req, res, next) => {
    console.log(req.ip)
    console.log(req.method, decodeURI(req.originalUrl))
    console.log("Cookies", req.cookies)
    console.log("Headers", req.headers)
    console.log("Query", req.query)
    console.log("Body", req.body)
    console.log("\n\n")
    next();
})
app.all('*', function (req, res) {
    res.status(200).send({ status: "received " + req.method + " " + decodeURI(req.originalUrl) });
})

app.listen(7700, () => {
    console.log("listening on 7700")
})