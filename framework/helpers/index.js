"use strict";
// exports.__esModule = true;

function tripleParse(parsee, params, recursive) {

    if (parsee instanceof Array) {
        parsee.forEach((element, i) => {
            parsee[i] = tripleParse.call(this, parsee[i], params, true)
        });
    }
    else if (typeof parsee == "string") {
        //parse all of the json variables declared 
        var variables = parsee.match(/{{{([^}]{0,}[^}]{0,}[^}]{0,})+}}}/g);
        if (variables) {
            //these two if statements check to see if the string is just a variable in itself
            //e.g {{{myVariable}}} as opposed to a string containing a variable
            //this will allow the parsee to retain the intended type instead of be converted to a string
            if (parsee.match(/}}}/g).length == 1 && parsee.match(/{{{/g).length == 1) {
                if (parsee.indexOf("{{{") == 0 && parsee.indexOf("}}}") == (parsee.length - 3)) {
                    //if so extract the one variable name removing {{{ }}} 
                    let varname = variables[0].replace("{{{", "").replace("}}}", "").trim()
                    //check for an empty variable between {{{ }}}
                    if (varname.length > 0) {
                        //check if this variable is supposed to be extracted from the credentials
                        if (varname.match(/:[^:]{1,}:/)) {
                            if (varname.match(/:[^:]{1,}:/)[0] == ":creds:" && varname.indexOf(":creds:") == 0) {
                                parsee = this.creds[varname.replace(":creds:", "")]
                            } else if (varname.match(/:[^:]{1,}:/)[0] == ":local:" && varname.indexOf(":local:") == 0) {
                                parsee = this.parsingVariables[varname.replace(":local:", "")]
                            }
                        } else {
                            parsee = params[varname]
                        }
                    } else {
                        parsee = '';
                    }
                }
            }
            else {
                //if the parsee is not just a single element then cycle through the string
                //and replace each of the variables one by one
                variables.forEach((element, i) => {
                    let varname = element.replace("{{{", "").replace("}}}", "").trim()

                    if (varname.length > 0) {
                        if (varname.match(/:[^:]{1,}:/)) {
                            if (varname.match(/:[^:]{1,}:/)[0] == ":creds:" && varname.indexOf(":creds:") == 3) {
                                parsee = parsee.replace(element, this.creds[varname.replace(":creds:", "")])
                            } else if (varname.match(/:[^:]{1,}:/)[0] == ":local:" && varname.indexOf(":local:") == 0) {
                                parsee = parsee.replace(element, this.parsingVariables[varname.replace(":local:", "")])
                            }
                            else {
                                parsee = parsee.replace(element, params[varname])
                            }
                        }
                    } else {
                        parsee = parsee.replace(element, "")
                    }
                });
            }
        }
        return parsee
    }

    //recursively parse all object properties
    else if (typeof parsee == "object") {
        for (let prop in parsee) {
            parsee[prop] = tripleParse.call(this, parsee[prop], params, true)
        }
    }

    return parsee;
}

function urlify(...params) {
    if (params.length == 0) {
        throw new Error("urlify must be provided with at least one argument")
    }
    if (typeof params[0] !== "string") {
        throw new Error("urlify's first paramter(hostname) must always be a string")
    }
    params.forEach(function (element, i) {
        if (typeof params[i] == "number") {
            params[i] += ""
        } else if (typeof params[i] != "string") {
            throw new TypeError("urilify only accepts strings and numbers to compose the url")
        }
    })
    let qparams = '';
    var url = '';
    for (let i = 1; i < params.length; i++) {
        if (params[i].indexOf('?') == 0 || params[i].indexOf('&') == 0) {
            if (qparams.indexOf('?') == -1) {
                qparams += `?${params[i].substr(1, params[i].length - 1)}`
            } else {
                qparams += `&${params[i].substr(1, params[i].length - 1)}`

            }
        }
        else if (params[i].indexOf('/') != 0) {
            url += `/${params[i]}`;
        } else {
            url += `${params[i]}`;
        }

    }
    if (params[0].indexOf("http://") != 0 && params[0].indexOf("https://") != 0) {
        params[0] = `http://${params[0]}`
    }
    var doubleSlash = /\/\//g;
    url = url.replace(doubleSlash, '/');
    url = `${params[0]}${url}`
    if (url.indexOf('?') > -1) {
        qparams = qparams.replace('?', '&')
    }
    url += qparams;
    if ((url.lastIndexOf('/') != url.length - 1) && (url.indexOf('?') === -1)) {
        url = url + '/';
    }
    return url;
}

class MergeSort {
    constructor(){

    }
    // Merges two subarrays of arr[].
    // First subarray is arr[l..m]
    // Second subarray is arr[m+1..r]
    merge(arr, l, m, r) {
        // Find sizes of two subarrays to be merged
        var n1 = m - l + 1;
        var n2 = r - m;

        /* Create temp arrays */
        var L = new Array(n1);
        var R = new Array(n2);

        /*Copy data to temp arrays*/
        for (let i = 0; i < n1; ++i)
            L[i] = arr[l + i];
        for (let j = 0; j < n2; ++j)
            R[j] = arr[m + 1 + j];


        /* Merge the temp arrays */

        // Initial indexes of first and second subarrays
        var i = 0, j = 0;

        // Initial index of merged subarry array
        var k = l;
        while (i < n1 && j < n2) {
            if (L[i].len <= R[j].len) {
                arr[k] = L[i];
                i++;
            }
            else {
                arr[k] = R[j];
                j++;
            }
            k++;
        }

        /* Copy remaining elements of L[] if any */
        while (i < n1) {
            arr[k] = L[i];
            i++;
            k++;
        }

        /* Copy remaining elements of R[] if any */
        while (j < n2) {
            arr[k] = R[j];
            j++;
            k++;
        }
        return arr;
    }

    // Main function that sorts arr[l..r] using
    // merge()
    sort(arr, l, r) {
        if (l < r) {
            // Find the middle point
            var m = Math.floor((l + r) / 2);

            // Sort first and second halves
            this.sort(arr, l, m);
            this.sort(arr, m + 1, r);

            // Merge the sorted halves
            this.merge(arr, l, m, r);
        }
        return arr
    }
}

module.exports = {
    urlify,
    tripleParse,
    MergeSort,
    __esModule: true
};