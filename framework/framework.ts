
import { Subject } from "rxjs/Subject"
import * as Ajv from "ajv"
import { MergeSort } from "./helpers/index"
// var Ajv = require('ajv');
var ajv = new Ajv({ $data: true, format: false });

function subParse(parsee, local) {
    if (typeof parsee == "object") {
        for (let prop in parsee) {
            subParse(parsee[prop], local)
        }
    } else if (typeof parsee == "string") {

    }
}



// ================================================================================================================================
//                                  TRIPPLE PARSE
// ================================================================================================================================
function tripleParse(parsee, params, recursive) {

    if (parsee instanceof Array) {
        parsee.forEach((element, i) => {
            parsee[i] = tripleParse.call(this, parsee[i], params, true)
        });
    }
    else if (typeof parsee == "string") {
        //parse all of the json variables declared 
        var variables = parsee.match(/{{{([^}]{0,}[^}]{0,}[^}]{0,})+}}}/g);
        if (variables) {
            //these two if statements check to see if the string is just a variable in itself
            //e.g {{{myVariable}}} as opposed to a string containing a variable
            //this will allow the parsee to retain the intended type instead of be converted to a string
            if (parsee.match(/}}}/g).length == 1 && parsee.match(/{{{/g).length == 1) {
                if (parsee.indexOf("{{{") == 0 && parsee.indexOf("}}}") == (parsee.length - 3)) {
                    //if so extract the one variable name removing {{{ }}} 
                    let varname = variables[0].replace("{{{", "").replace("}}}", "").trim()
                    //check for an empty variable between {{{ }}}
                    if (varname.length > 0) {
                        //check if this variable is supposed to be extracted from the credentials
                        if (varname.match(/:[^:]{1,}:/)) {
                            if (varname.match(/:[^:]{1,}:/)[0] == ":creds:" && varname.indexOf(":creds:") == 0) {
                                parsee = this._.creds[varname.replace(":creds:", "")]
                            } else if (varname.match(/:[^:]{1,}:/)[0] == ":local:" && varname.indexOf(":local:") == 0) {
                                parsee = this._.parsingVariables[varname.replace(":local:", "")]
                            }
                        } else {
                            parsee = params[varname]
                        }
                    } else {
                        parsee = '';
                    }
                }
            }
            else {
                //if the parsee is not just a single element then cycle through the string
                //and replace each of the variables one by one
                variables.forEach((element, i) => {
                    let varname = element.replace("{{{", "").replace("}}}", "").trim()

                    if (varname.length > 0) {
                        if (varname.match(/:[^:]{1,}:/)) {
                            if (varname.match(/:[^:]{1,}:/)[0] == ":creds:" && varname.indexOf(":creds:") == 3) {
                                parsee = parsee.replace(element, this._.creds[varname.replace(":creds:", "")])
                            } else if (varname.match(/:[^:]{1,}:/)[0] == ":local:" && varname.indexOf(":local:") == 0) {
                                parsee = parsee.replace(element, this._.parsingVariables[varname.replace(":local:", "")])
                            }
                            else {
                                parsee = parsee.replace(element, params[varname])
                            }
                        }
                    } else {
                        parsee = parsee.replace(element, "")
                    }
                });
            }
        }
        return parsee
    }

    //recursively parse all object properties
    else if (typeof parsee == "object") {
        for (let prop in parsee) {
            parsee[prop] = tripleParse.call(this, parsee[prop], params, true)
        }
    }

    return parsee;
}
// ================================================================================================================================
//                                  TRIPPLE PARSE
// ================================================================================================================================


// ================================================================================================================================
//                                              URLIFY
// ================================================================================================================================
function urlify(...params) {
    let qparams = '';
    var url = '';
    for (let i = 1; i < params.length; i++) {
        if (params[i].indexOf('?') == 0 || params[i].indexOf('&') == 0) {
            if (qparams.indexOf('?') == -1) {
                qparams += `?${params[i].substr(1, params[i].length - 1)}`
            } else {
                qparams += `&${params[i].substr(1, params[i].length - 1)}`

            }
        }
        else if (params[i].indexOf('/') != 0) {
            url += `/${params[i]}`;
        } else {
            url += `${params[i]}`;
        }

    }
    if (params[0].indexOf("http://") != 0 && params[0].indexOf("https://") != 0) {
        params[0] = `http://${params[0]}`
    }
    var doubleSlash = /\/\//g;
    url = url.replace(doubleSlash, '/');
    url = `${params[0]}${url}`
    if (url.indexOf('?') > -1) {
        qparams = qparams.replace('?', '&')
    }
    if ((url.lastIndexOf('/') === url.length - 1)) {
        // so we dont mess up the query parameters to come
        url = url.replace(/\/$/, '')
    }
    url += qparams;
    if ((url.lastIndexOf('/') != url.length - 1) && (url.indexOf('?') === -1)) {
        url = url + '/';
    }
    // return encodeURI(url);
    return (url);
}
// ================================================================================================================================
//                                              URLIFY
// ================================================================================================================================

// ================================================================================================================================
//                                              GETREF
// ================================================================================================================================
function getRef(init, ref, docSpec) {
    if (docSpec === "discovery") {
        if (init['schemas'] !== undefined && init['schemas'][ref] !== undefined) {
            return init['schemas'][ref]
        }
        return
    }
    var parts = ref.split("/");
    var referencedObject
    if (parts.indexOf("#") === 0) {
        referencedObject = init;
    } else if (parts[0].split("#")[0].endsWith(".json")) {
        referencedObject = require(parts[0].split("#")[0]);
    } else if (parts[0].split("#")[0].endsWith(".yaml")) {
        throw new Error("Sorry, yaml references not yet supported")
    } else {
        throw new Error(`reference '${ref}' invalid`)
    }
    for (let i = 1; i < parts.length; i++) {
        if (referencedObject !== undefined && referencedObject[parts[i]]) {
            referencedObject = referencedObject[parts[i]];
        } else {
            throw new ReferenceError(`The referenced objeect '${ref}' does not exist at property ${parts[i]}`)
        }

    }
    return referencedObject;
}
// ================================================================================================================================
//                                              GETREF
// ================================================================================================================================
// ================================================================================================================================
//                                                 DEREFMAKER
// ================================================================================================================================
function deRefMaker(init, spec) {
    return function inside(myObject) {
        if (myObject instanceof Object) {
            if ("$ref" in myObject) {
                var refPath = myObject["$ref"];
                delete myObject["$ref"];

                return Object.assign(myObject, getRef(init, refPath, spec));
            }
        } else if (myObject instanceof Array) {
            myObject.forEach((element, index) => {
                if (typeof element === "object") {
                    myObject[index] = inside(myObject[index])
                }
            })
        }
        for (let prop in myObject) {
            if (typeof myObject[prop] === "object") {
                myObject[prop] = inside(myObject[prop])
            }
        }
        if (Object.keys(myObject).length === 0) {
            return myObject;
        }

        return myObject;
    }
}
// ================================================================================================================================
//                                                 DEREFMAKER
// ================================================================================================================================

// ================================================================================================================================
//                                                 WAO
// ================================================================================================================================
class WAO {
    private _priv: {
        __temp: any,
        initData: any,
        requestObjects: XMLHttpRequest[]

    }
    _: {
        hostname: String,
        spec: String,
        parsingVariables: any,
        OASServers: any,
        reg: any,
        singleMatchReg,
        //CANT POSSIBLY STAY HERE
        creds,
        token: String

    }
    run: any;


    constructor({ init, creds = {}, spec = "n/a" }) {
        //PASS IN PATHS TO THE CONFIG FILES INSTEAD OF THE DATA
        var __this = this
        var myDereferencer = deRefMaker(init, spec)

        __this['_priv'] = {
            __temp: {},
            initData: myDereferencer(init),
            requestObjects: []
        };
        this['_'] = {};
        __this._priv.initData = myDereferencer(init)

        __this._.spec = spec;
        __this._.reg = RegExp('\{[^\}]{1,}\}')
        __this._.singleMatchReg = RegExp('^\{[^\}]{1,}\}$')
        __this._priv.requestObjects = [];
        __this._.OASServers = [];
        __this.run = {};
        var sort = new MergeSort();


        if (spec === "oas3.0") {
            init.servers.forEach((element) => {
                // TODO: url can be relative to the location of the OAS file 
                var ind = __this._.OASServers.push({
                    url: element.url
                }) - 1;
                if (element.variables) {
                    __this._.OASServers[ind].variables = {};
                    for (let prop in element.variables) {
                        __this._.OASServers[ind].variables[prop] = { default: element.variables[prop].default };
                        (element.variables[prop].enum) ? (__this._.OASServers[ind].variables[prop].enum = element.variables[prop].enum) : null;

                    }

                }
            })

            var urisWithParam = [];
            var urisWithoutParam = [];
            for (let path in init.paths) {
                // VALIDATE PATH AGAINST SCHEMA !!!!!!!!
                let segments = path.split('/');
                let checkee = __this;
                let temp = false;


                if (__this._.reg.test(path)) {
                    urisWithParam.push({ path, len: (path.split('/').length) })
                } else {
                    urisWithoutParam.push(path);

                }

            }
            // ================================================================================================================================

            __this.endPointCycler(urisWithParam, urisWithoutParam, __this)
        }
        // ================================================================================================================================

        // ================================================================================================================================
        //                                                 DISCOVERY DOCS
        // ================================================================================================================================

        else if (spec === "discovery") {
            var __this = this;
            __this._.hostname = init.rootUrl;
            __this['servicePath'] = init.servicePath
            let pathUris = []
            let nonPathUris = [];

            (function searchDisc(init, noPaths, paths, loc = 'init.resources') {
                for (const resource in init['resources']) {
                    if ('resources' in init['resources'][resource]) {
                        searchDisc(init['resources'][resource], noPaths, paths, loc + '.' + resource + '.resources')
                    }
                    for (let method in init['resources'][resource]["methods"]) {
                        let initPath = init['resources'][resource]["methods"][method]['flatPath'];
                        initPath = (/:[^:]{1,}$/.test(initPath)) ? (initPath.slice(0, initPath.indexOf(':'))) : initPath;
                        // below we add empty resources to opDetails because it is unnecessary to store nested resources
                        if (__this._.reg.test(initPath)) {
                            paths.push({ opDetails: Object.assign(init['resources'][resource]["methods"][method], { description: null }), path: initPath, len: initPath.split('/').length })
                        } else {
                            noPaths.push({ opDetails: Object.assign(init['resources'][resource]["methods"][method], { description: null }), path: initPath })
                        }
                    }
                }
            }(init, nonPathUris, pathUris));
            __this.endPointCycler(pathUris, nonPathUris, __this)
        }

        // ================================================================================================================================
        //                                                 END DISCOVERY DOCS
        // ================================================================================================================================
        else if (typeof init == "object") {
            var __this = this;
            __this._.hostname = init.host;//host
            __this._.creds = creds;
            __this._.parsingVariables = init.local;

            var endpoints = init.endpoints;//known endpoints
            var authData = (init.authInfo) ? init.authInfo.authData : null;//required auth data
            var functions = init.functions;



            // __this["func"] = {};
            //iterate through all the predefined functions and attach those to the object
            for (let prop in functions) {
                __this[prop] = function (params) {
                    var funcThis = this;
                    //parse the object and replace place holders with their values
                    functions[prop] = tripleParse.call(__this, functions[prop], params, undefined);
                    let method = functions[prop]["method"];
                    var xhr = new XMLHttpRequest();

                    //if the user specifies query parameters, then grab them and attach them to the url
                    var paramArray = [];
                    if (params.queryparams && functions[prop]["qparams"]) {
                        for (let param in params.queryparams) {
                            if (functions[prop]["qparams"].indexOf(param) > -1) {
                                paramArray.push(`?${param}=${params.queryparams[param]}`)
                            }
                        }
                    }
                    let url = urlify(__this._.hostname, functions[prop]["uri"], ...paramArray)
                    xhr.open(method, url, true)
                    for (let header in functions[prop]["headers"]) {
                        xhr.setRequestHeader(header, functions[prop]["headers"][header])
                    }
                    if (method == "POST" || method == "PUT" || method == "PATCH") {
                        var body = functions[prop]["body"]
                    }

                    var index;
                    // this.complete = false;
                    if (this[prop].xObjects === undefined) {
                        this[prop].xObjects = [];
                    }
                    index = this[prop].xObjects.push(xhr) - 1;
                    this[prop][index] = {
                        index,
                        subject: new Subject(),
                        complete: false
                    }
                    this[prop].xObjects[index].onreadystatechange = function () {
                        if (this.readyState == 4 && this.status == 200) {

                            //pass the data to the rxjs object
                            funcThis[prop][index]["subject"].next(JSON.parse(this.responseText))
                            funcThis[prop][index]["subject"].complete()
                        } else if (this.readyState == 4 && this.status > 399 && this.status < 600) {
                            funcThis[prop][index]["subject"].error(this.responseText)

                        } else if (this.readyState == 4) {
                            funcThis[prop][index]["subject"].error(this.status + " " + this.responseText)

                        }
                    }

                    this[prop].xObjects[index].send(JSON.stringify(body))
                    __this._priv.requestObjects.push(xhr)
                    return this[prop][index].subject.asObservable().toPromise();

                }

            }



            //iterate through all the endpoints specified in the config  and add its name as a property on the 'WAO' object
            // for (let prop in endpoints) {
            //     this[prop] = {
            //         //on the resulting property object, add the following functions

            //         //add this function will be used by the programmer to add another item to the collection
            //         //e.g calling this on a 'players' endpoint to add another players data
            //         add(data: Object) {
            //             var stamp = new Date().getTime();
            //             var index;
            //             let url = urlify(_this._.hostname, endpoints[prop].route)

            //             //the following steps are the same as explained for the 'getHelper' function
            //             index = _this.requestObjects.push(new XMLHttpRequest()) - 1;
            //             _this[prop][stamp] = {
            //                 index: index,
            //                 subject: new Subject()
            //             };


            //             _this.requestObjects[index].onreadystatechange = function () {
            //                 if (this.readyState == 4 && (this.status >= 200 && this.status < 300)) {
            //                     _this[prop][stamp]["subject"].complete("success")
            //                 } else if (this.readyState == 4 && !(this.status >= 200 && this.status < 300)) {
            //                     _this[prop][stamp]["subject"].error(this.responseText)
            //                 }
            //             }

            //             _this.requestObjects[index].open("POST", `http://${_this._.hostname}${endpoints[prop].route}`, true);
            //             if (endpoints[prop]["authed"]) {
            //                 authData.headers.forEach(element => {
            //                     _this.requestObjects[index].setRequestHeader(element, creds[element]);
            //                 });
            //             }
            //             _this.requestObjects[index].setRequestHeader("Content-type", "application/json");
            //             _this.requestObjects[index].send(JSON.stringify(data));

            //             return _this[prop][stamp]["subject"].asObservable().toPromise();

            //         },
            //         sub(params) {
            //             //this methos uses the gethelper to return a subscription to the endpoint
            //             return _this.getHelper(prop, params, false, {
            //                 route: endpoints[prop].route,
            //                 authed: endpoints[prop]["authed"],
            //                 authData: authData,
            //                 creds: creds
            //             })
            //         },
            //         get(params) {
            //             //this methos uses the gethelper to return a promise fullfiling on request completion
            //             return _this.getHelper(prop, params, true, {
            //                 route: endpoints[prop].route,
            //                 authed: endpoints[prop]["authed"],
            //                 authData: authData,
            //                 creds: creds
            //             })

            //         },
            //         ref(path) {

            //             //return a ref, function not defined here because it can also be used on the base object as well
            //             //makes it easier to have in one place as I might add stuff as well
            //             return _this.ref(`${endpoints[prop].route}${path}`)
            //         }
            //     }//END attaching of all functions for current proerty
            // }// END for '' in
        }

    }

    private getHelper(prop, params, asPromise: boolean, { route, authed, authData, creds }) {
        var __this = this;
        if (typeof params == "string") {

            //combine _.hostname, route to the api and the params to get a valid url.
            let url = urlify(__this._.hostname, route, params)

            //index where the xhr object will reside in the array
            var index;

            //check to see if any web requests were made to this path already and therefore if this property was created already
            if (!__this[prop][params]) {

                //store index of http object which is pushed on to the array holding ALL XMLobjects made using this object 
                index = __this._priv.requestObjects.push(new XMLHttpRequest()) - 1;

                //on the property related to the URI path, store the XMLobject's index in requestObjects
                //also store a new rxjs subject to be used to deliver the data
                __this[prop][params] = {
                    index: index,
                    subject: new Subject()
                };

            } else {
                //if the property already existed then just get the index to the XMLobject
                index = __this[prop][params].index;
            }

            //function to run when the data is returned
            __this._priv.requestObjects[index].onreadystatechange = function () {

                //when request is finished and we get a 200 response
                if (this.readyState == 4 && this.status >= 200 && this.status < 300) {

                    //pass the data to the rxjs object
                    __this[prop][params]["subject"].next(JSON.parse(this.responseText))
                    __this[prop][params]["subject"].complete()
                }
                else if (this.readyState == 4 && !(this.status >= 200 && this.status < 300)) {

                    //pass the error to thr rxjs object
                    __this[prop][params]["subject"].error({status: this.status, message: JSON.parse(this.responseText)})
                }
            };
            var returnVal;

            //depending on wether we want to return a promise or not, we assign the rxjs object to the return value
            //if we do want a promise we convert it to a promise here
            if (asPromise) {
                returnVal = __this[prop][params]["subject"].asObservable().toPromise();
            } else {
                returnVal = __this[prop][params]["subject"].asObservable();
            }

            //open request using the object stored at the correct index
            __this._priv.requestObjects[index].open("GET", url, true);

            //if the config indicates that this URI requires authentication, then add the relevent headers indicated by authData
            //(TODO: support to be added for other types of auth)
            if (authed) {
                authData.headers.forEach(element => {
                    __this._priv.requestObjects[index].setRequestHeader(element, creds[element]);
                });
            }

            //finally send the request
            __this._priv.requestObjects[index].send();

            return returnVal;
        }
        else {
            return "NOT YET IMPLEMENTED!"
        }
    }
    // ================================================================================================================================
    //                                                 OPERATION GENERATOR
    // ================================================================================================================================
    private funcGenerator(type, path, substitutedPath, ref) {
        var __this = this;
        var uripath;
        if (substitutedPath !== undefined) {
            uripath = substitutedPath;
        } else {
            uripath = path;
        }
        return function currentGet({ body, headers = {}, queryparams = {}, cookies = {} } = {}) {
            let subjIndex;
            let rObjectIndex;
            var infoStore = {
                method: type,
                url: '',
                qparams: [],
                headers: [],
                body: {}
            }

            currentGet['path'] = uripath;
            let url = __this._.OASServers[0].url + uripath;
            if (__this._priv.initData["paths"][path][type]["deprecated"] === true) {
                console.warn(`The ${path} endpoint using ${type} method is deprecated, please update your code`)
            }

            // TODO: THE BELOW METHODOLOGY WILL ALMOST CERTAINLY CAUSE PROBLEMS WITH CONCURRENT REQUEST TO THE SAME ENDPOINT/METHOD

            //check to see if any web requests were made to this path already and therefore if this property was created already
            if (currentGet['requestData'] === undefined) {
                currentGet['requestData'] = []
            }
            //store index of http object which is pushed on to the array holding ALL XMLobjects made using this object 
            rObjectIndex = __this._priv.requestObjects.push(new XMLHttpRequest()) - 1;

            //on the property related to the URI path, store the XMLobject's index in requestObjects
            //also store a new rxjs subject to be used to deliver the data
            subjIndex = currentGet['requestData'].push({
                rObjectIndex,
                subject: new Subject()
            }) - 1;


            __this._priv.requestObjects[rObjectIndex].onreadystatechange = function () {

                //when request is finished and we get a 200 response
                if (this.readyState == 4 && this.status >= 200 && this.status < 300) {

                    //pass the data to the rxjs object
                    currentGet['requestData'][subjIndex]["subject"].next(JSON.parse(this.responseText))
                    currentGet['requestData'][subjIndex]["subject"].complete()
                }
                else if (this.readyState == 4 && !(this.status >= 200 && this.status < 300)) {

                    //pass the error to thr rxjs object
                    currentGet['requestData'][subjIndex]["subject"].error({status: this.status, message: JSON.parse(this.responseText)})
                }
            };

            //MUST HAPPEN BEFORE THE REQUEST IS OPENED!!
            if (__this._priv.initData.paths[path][type]['parameters']) {

                __this._priv.initData.paths[path][type]['parameters'].forEach((paramObject, index) => {
                    let currentParam = __this._priv.initData.paths[path][type]['parameters'][index]
                    if (currentParam['in'] === 'query') {
                        if (queryparams[currentParam['name']] === undefined && currentParam['required'] && currentParam['required'] === true) {
                            throw new Error("Missing required query parameter " + currentParam['name'])
                        }
                        if (queryparams[currentParam['name']] !== undefined) {
                            if ('schema' in currentParam) {
                                var queryValidator = ajv.compile(currentParam['schema'])
                                var validQuery = queryValidator(queryparams[currentParam['name']])
                                if (!validQuery) {
                                    var queryMessage = "";
                                    queryValidator.errors.forEach((error) => {
                                        queryMessage += `Error at queryparams.${currentParam['name'] + error.dataPath} ${(error.message === undefined) ? '' : `with message ${error.message}`}.\n`
                                        throw new Error("The query parameter doesn't match the specified schema\n" + queryMessage)
                                    })
                                }
                            }
                            if (queryparams[currentParam['name']] instanceof Array) {
                                if (currentParam['explode'] === true) {
                                    queryparams[currentParam['name']].forEach((element, index) => {
                                        let starter;
                                        starter = (url.indexOf('?') > -1) ? `&` : `?`;
                                        url = `${url + starter}${(currentParam['name'])}=${(queryparams[currentParam['name']][index])}`;
                                    });
                                } else {
                                    let starter;
                                    starter = (url.indexOf('?') > -1) ? `&` : `?`;
                                    url = `${url + starter}${currentParam['name']}=${(queryparams[currentParam['name']].join(','))}`;
                                }
                            } else if (queryparams[currentParam['name']] instanceof Object) {
                                if (currentParam['explode'] === true) {
                                    url = (url.indexOf('?') > -1) ? `${url}&${(currentParam['name'])}=` : `${url}?${(currentParam['name'])}=`;
                                    for (let prop of queryparams[currentParam['name']]) {
                                        url = `${url}${(prop)},${(queryparams[currentParam['name']][prop])}`;
                                    }
                                } else {
                                    for (let prop of queryparams[currentParam['name']]) {
                                        let starter;
                                        starter = (url.indexOf('?') > -1) ? `&` : `?`;
                                        url = `${url + starter}${(prop)}=${(queryparams[currentParam['name']][prop])}`;
                                    }
                                }
                            } else {
                                url = (url.indexOf('?') > -1) ? (`${url}&${currentParam['name']}=${queryparams[currentParam['name']]}`) : (`${url}?${currentParam['name']}=${queryparams[currentParam['name']]}`);
                            }
                        }

                    }
                })
            }
            __this._priv.requestObjects[rObjectIndex].open(type, urlify(url), true);
            infoStore.url = urlify(url)

            //MUST HAPPEN AFTER THE REQUEST IS OPENED!!
            if (__this._priv.initData.paths[path][type]['parameters']) {
                __this._priv.initData.paths[path][type]['parameters'].forEach((paramObject, currI) => {
                    let currentParam = __this._priv.initData.paths[path][type]['parameters'][currI]
                    if ('deprecated' in currentParam && currentParam['deprecated']) {
                        console.warn(`The ${currentParam['in']} parameter '${currentParam['name']}' is deprecated.`)
                    }
                    if (currentParam['in'] === 'header') {
                        if (headers[currentParam['name']] === undefined && currentParam['required'] && currentParam['required'] === true) {
                            throw new Error("Missing required header " + currentParam['name'])
                        }
                        if (headers[currentParam['name']] !== undefined) {
                            if ('schema' in currentParam) {
                                var validator = ajv.compile(currentParam['schema'])
                                var validParam = validator(headers[currentParam['name']])
                                if (!validParam) {
                                    var paramMessage = "";
                                    validator.errors.forEach((error) => {
                                        paramMessage += `Error at headers${error.dataPath} ${(error.message === undefined) ? '' : `with message ${error.message}`}.\n`
                                    })
                                    throw new Error("The header " + currentParam['name'] + " doesn't match the specified schema\n" + paramMessage)
                                }
                            }
                            __this._priv.requestObjects[rObjectIndex].setRequestHeader(currentParam['name'], headers[currentParam['name']])
                            infoStore.headers.push({ name: currentParam['name'], val: headers[currentParam['name']] })
                        }
                    } else if (currentParam['in'] === 'cookie') {
                        if (queryparams[currentParam['name']] === undefined && currentParam['required'] && currentParam['required'] === true) {
                            throw new Error("Missing required cookie " + currentParam['name'])
                        }
                        if ('schema' in currentParam) {
                            var cookieValidator = ajv.compile(currentParam['schema'])
                            var validCookie = cookieValidator(cookies[currentParam['name']])
                            if (!validCookie) {
                                var cookieMessage = "";
                                cookieValidator.errors.forEach((error) => {
                                    cookieMessage += `Error at cookies${error.dataPath} ${(error.message === undefined) ? '' : `with message${error.message}`}.\n`
                                })
                                throw new Error("The cookie doesn't match the specified schema\n" + cookieMessage)
                            }
                        }
                        // TODO:   must implement something for cookies
                    }
                })
            }

            if (type !== 'get' && type !== 'delete') {
                if (__this._priv.initData.paths[path][type]['requestBody'] !== undefined && __this._priv.initData.paths[path][type]['requestBody']['required'] && __this._priv.initData.paths[path][type]['requestBody']['required'] === true) {
                    if (body === undefined) {
                        throw new Error("Missing required request body")
                    } else {
                        var validate = ajv.compile(__this._priv.initData.paths[path][type]['requestBody']["content"]["application/json"]["schema"]);
                        var valid = validate(body);
                        if (!valid) {
                            let errMessage = "";
                            validate.errors.forEach((error) => {
                                errMessage += `Error at body${error.dataPath} ${(error.message === undefined) ? '' : `with message ${error.message}`}.\n`
                            })
                            throw new Error('The following error(s) occured\n' + errMessage)
                        } else {

                        };
                    }
                }
            } else {
                body = null
            }
            if (body !== null && body !== undefined) {
                __this._priv.requestObjects[rObjectIndex].setRequestHeader("Content-Type", "application/json")
                __this._priv.requestObjects[rObjectIndex].send(JSON.stringify(body));
                infoStore.headers.push({ name: "Content-Type", val: "application/json" })
                infoStore.body = body

            } else {
                __this._priv.requestObjects[rObjectIndex].send();
            }

            if (ref) {
                return new Instance(infoStore);
            } else {
                return currentGet['requestData'][subjIndex]["subject"].asObservable().toPromise();
            }

        }
    }

    // ================================================================================================================================
    //                                                 OPERATION GENERATOR
    // ================================================================================================================================


    // ================================================================================================================================
    //                                                 OASMETHOD ATTACHER
    // ================================================================================================================================

    private oasMethodAttacher(path, destination, actualPath) {
        var __this = this
        if (typeof path === "object") {
            path = path.path
        }
        // A PROPERTY LOOP MIGHT BE FASTER SINCE EACH OF THESE IFS MIGHT LOOP THE OBJECT
        if ('get' in __this._priv.initData.paths[path]) {
            let pathOperation = __this.funcGenerator("get", path, actualPath, false)
            destination.get = pathOperation;
            destination.get.$ref = __this.funcGenerator("get", path, actualPath, true)
            if ('operationId' in __this._priv.initData.paths[path]["get"]) {
                __this["run"][__this._priv.initData.paths[path]["get"]["operationId"]] = pathOperation;
            }
        }
        if ('put' in __this._priv.initData.paths[path]) {
            let pathOperation = __this.funcGenerator("put", path, actualPath, false)
            destination.put = pathOperation;
            destination.put.$ref = __this.funcGenerator("put", path, actualPath, true)
            if ('operationId' in __this._priv.initData.paths[path]["put"]) {
                __this["run"][__this._priv.initData.paths[path]["put"]["operationId"]] = pathOperation;
            }
        }
        if ('post' in __this._priv.initData.paths[path]) {
            let pathOperation = __this.funcGenerator("post", path, actualPath, false)
            destination.post = pathOperation;
            destination.post.$ref = __this.funcGenerator("post", path, actualPath, true)
            if ('operationId' in __this._priv.initData.paths[path]["post"]) {
                __this["run"][__this._priv.initData.paths[path]["post"]["operationId"]] = pathOperation;
            }
        }
        if ('delete' in __this._priv.initData.paths[path]) {
            let pathOperation = __this.funcGenerator("delete", path, actualPath, false)
            destination.delete = pathOperation;
            destination.delete.$ref = __this.funcGenerator("delete", path, actualPath, true)
            if ('operationId' in __this._priv.initData.paths[path]["delete"]) {
                __this["run"][__this._priv.initData.paths[path]["delete"]["operationId"]] = pathOperation;
            }
        }
        if ('options' in __this._priv.initData.paths[path]) {
            let pathOperation = __this.funcGenerator("options", path, actualPath, false)
            destination.options = pathOperation;
            destination.options.$ref = __this.funcGenerator("options", path, actualPath, true)
            if ('operationId' in __this._priv.initData.paths[path]["options"]) {
                __this["run"][__this._priv.initData.paths[path]["options"]["operationId"]] = pathOperation;
            }
        }
        if ('head' in __this._priv.initData.paths[path]) {
            let pathOperation = __this.funcGenerator("head", path, actualPath, false)
            destination.head = pathOperation;
            destination.head.$ref = __this.funcGenerator("head", path, actualPath, true)
            if ('operationId' in __this._priv.initData.paths[path]["head"]) {
                __this["run"][__this._priv.initData.paths[path]["head"]["operationId"]] = pathOperation;
            }
        }
        if ('patch' in __this._priv.initData.paths[path]) {
            let pathOperation = __this.funcGenerator("patch", path, actualPath, false)
            destination.patch = pathOperation;
            destination.patch.$ref = __this.funcGenerator("patch", path, actualPath, true)
            if ('operationId' in __this._priv.initData.paths[path]["patch"]) {
                __this["run"][__this._priv.initData.paths[path]["patch"]["operationId"]] = pathOperation;
            }
        }
        if ('trace' in __this._priv.initData.paths[path]) {
            let pathOperation = __this.funcGenerator("trace", path, actualPath, false)
            destination.trace = pathOperation;
            destination.trace.$ref = __this.funcGenerator("trace", path, actualPath, true)
            if ('operationId' in __this._priv.initData.paths[path]["trace"]) {
                __this["run"][__this._priv.initData.paths[path]["trace"]["operationId"]] = pathOperation;
            }
        }
    }

    // ================================================================================================================================
    //                                                 END OASMETHOD ATTACHER
    // ================================================================================================================================

    discFuncGenerator(info, uri) {

        return function reqSender({ }) {

        }
    }

    // ================================================================================================================================
    //                                                 DISCMETHOD ATTACHER
    // ================================================================================================================================

    private discMethodAttacher(info, destination, uri) {
        var __this = this
        var method = info.opDetails.httpMethod
        let infoStore = {};
        uri = urlify(__this._.hostname + uri)


        destination[info.opDetails.httpMethod.toLowerCase()] = function reqSender({ queryparams = {} } = {}, ref) {
            if (__this.discMethodAttacher['requestData'] === undefined) {
                __this.discMethodAttacher['requestData'] = []
            }
            var reqIndex = __this._priv.requestObjects.push(new XMLHttpRequest()) - 1;

            var subjIndex = __this.discMethodAttacher['requestData'].push({
                reqIndex,
                subject: new Subject()
            }) - 1;


            for (let parameter in info.opDetails.parameters) {
                if (info.opDetails.parameters[parameter]['location'] === 'query') {
                    if (info.opDetails.parameters[parameter]['required'] === true && queryparams[parameter] === undefined && info.opDetails.parameters[parameter]['default'] === undefined) {
                        // if its required AND its not there AND there's no default we throw and error
                        throw new Error("Missing required query parameter " + parameter)
                    }
                    else if (queryparams[parameter] === undefined && info.opDetails.parameters[parameter]['default'] !== undefined) {
                        queryparams[parameter] = info.opDetails.parameters[parameter]['default']
                    }
                    if (queryparams[parameter] !== undefined) {
                        let queryValidator = ajv.compile(info.opDetails.parameters[parameter])
                        let isValid = queryValidator(queryparams[parameter])
                        if (!isValid) {
                            console.log(queryValidator.errors)
                            throw new Error("Error: query parameter failed schema check with error '" + queryValidator.errors[0].message + "'")
                        }
                        uri = urlify(uri, ((uri.indexOf('?') > 0) ? '&' : '?') + parameter + '=' + queryparams[parameter])
                    }
                }
            }


            __this._priv.requestObjects[reqIndex].onreadystatechange = function () {
                if (this.readyState == 4 && (this.status < 199 && this.status > 300)) {

                    //pass the data to the rxjs object
                    __this.discMethodAttacher['requestData'][subjIndex]["subject"].next(JSON.parse(this.responseText))
                    __this.discMethodAttacher['requestData'][subjIndex]["subject"].complete()
                }
                else if (this.readyState == 4 && !(this.status >= 200 && this.status < 300)) {

                    //pass the error to thr rxjs object
                    __this.discMethodAttacher['requestData'][subjIndex]["subject"].error({status: this.status, message: JSON.parse(this.responseText)})
                }
            }

            __this._priv.requestObjects[reqIndex].open(method, uri, true)
            __this._priv.requestObjects[reqIndex].setRequestHeader(`Content-Type`, `application/json`)
            
            __this._priv.requestObjects[reqIndex].send()
            infoStore.method = method;
            infoStore.url = uri;

            if (ref === true) {
                return new Instance(infoStore)
            } else {

                return __this.discMethodAttacher['requestData'][subjIndex]['subject'].asObservable().toPromise()
            }

        }

        destination[info.opDetails.httpMethod.toLowerCase()].$ref = function (params) {
            return destination[info.opDetails.httpMethod.toLowerCase()](params, true)
        }
    }

    // ================================================================================================================================
    //                                                 END DISCMETHOD ATTACHER
    // ================================================================================================================================

    // ================================================================================================================================
    //                                                 ENDPOINT CYCLER
    // ================================================================================================================================
    private endPointCycler(withParams, withoutParams, attachTo) {
        var Attacher;
        var __this = this;
        var sort = new MergeSort()
        if (this._.spec == "discovery") {
            // without binding, the this variable looses scope when the method is being passed into
            // the 'Attacher' variable even tho it is an object method, so a bind is used
            Attacher = this.discMethodAttacher.bind(this)
        } else if (this._.spec == 'oas3.0') {
            Attacher = this.oasMethodAttacher.bind(this)
        }
        if (withParams.length > 0) {
            withParams = sort.sort(withParams, 0, (withParams.length - 1))
            for (let i = 0; i < withParams.length; i++) {
                // for each uri with a path param
                var progress = attachTo
                var attachedToPathParam = false;
                let segments = withParams[i].path.split('/');
                var start;
                start = (segments[0] === "") ? 1 : 0;
                for (let j = start; j < segments.length; j++) {
                    // for each segment in this uri
                    if (!this._.singleMatchReg.test(segments[j])) {
                        // if this segment isnt the path param segment
                        if (this._.singleMatchReg.test(segments[j + 1])) {
                            // if the next property is a path param that means this needs to be function
                            //assign the relevant properties to this function AND!!!! its return value
                            if ((progress[segments[j]]) === undefined) {
                                // if the property defined by this segment is undefined for the target object
                                // then attach it here. (we already determined it will be a function)
                                progress[segments[j]] = (function genGen(start, replaceWith, currentSegIndex, currentSegs, pathsWithParams, opDetailsIndex) {
                                    return function pathPGen(pathParam) {
                                        // in this funciton we define the methods (e.g get, post) that can
                                        // be run on this property, as well as sub propreties, and attach them
                                        // to the return value

                                        if (pathParam === undefined || pathParam === "" || pathParam === "") {
                                            // path param must be included
                                            throw new Error("path parameter cannot be null, undefined or empty string")
                                        }
                                        let returnVal = {};
                                        let propRef = returnVal; // reference to go through deeper properties while still having a return value
                                        // finish this path first
                                        for (let k = currentSegIndex; k < currentSegs.length; k++) {
                                            // firstly we cycle through the remaining segments repeating the outter process
                                            // for (let j = 1; j < segments...) but this time we check that we need to attach methods 
                                            // instead of placeholders on the final iteration, since unlike the outter loop, it is 
                                            // perfectly possible that the break statement will not be executed
                                            if (!__this._.singleMatchReg.test(currentSegs[k])) {
                                                // make sure this isnt the path param that was implemented last iteration
                                                if (__this._.singleMatchReg.test(currentSegs[k + 1])) {
                                                    // what happens here is that if we get here from a recursive call and not the initial call, then we need to keep track of the previous path parameter that was passed and also what it was replacing,
                                                    // in order to do that we pass in the current path as well as the path with all values already substituted so that we know what to replace and what to replace it with for the next level, we also add our path substitution for the current level when making the replacements
                                                    propRef[currentSegs[k]] = genGen(currentSegs.slice(0, k + 1).join('/'), currentSegs.slice(0, k + 1).join('/').replace(start, replaceWith).replace(/\{[^\}]{1,}\}/, pathParam), k + 1, currentSegs, pathsWithParams, opDetailsIndex)
                                                    break;
                                                } else {
                                                    (propRef[currentSegs[k]] === undefined) ? (propRef[currentSegs[k]] = {}, propRef = propRef[currentSegs[k]]) : (propRef = propRef[currentSegs[k]]);
                                                    if (k == currentSegs.length - 1) {
                                                        Attacher(pathsWithParams[opDetailsIndex], propRef, pathsWithParams[opDetailsIndex].path.replace(start, replaceWith).replace(/\{[^\}]{1,}\}/, pathParam))
                                                    }
                                                }
                                            } else if (k == currentSegIndex) {
                                                // if we get here then the current segment matches a path param but this is the first iteration of 
                                                // the loop and therefore we need to attach those properties (operations) to the return value in here

                                                var fulldetails = pathsWithParams[opDetailsIndex];
                                                Attacher(fulldetails, returnVal, fulldetails.path.replace(start, replaceWith).replace(/\{[^\}]{1,}\}/, pathParam))
                                            }
                                        }
                                        // attach all other sub-related properties
                                        pathsWithParams.forEach((element, ind) => {
                                            if (ind > opDetailsIndex) {
                                                // in reality this isnt necessary just create a for loop starting at opDetailsIndex
                                                let newRef = returnVal;
                                                var newSegments = element.path.split('/')
                                                if (element.path.indexOf(pathsWithParams[opDetailsIndex].path) === 0) {
                                                    // if the property starts with the same path as out current property we need to attach it here
                                                    if (element.path === pathsWithParams[opDetailsIndex].path) {
                                                        // this is true when already worked on a path e.g players/{playerId}
                                                        // and come across that same path again since every individual operation 
                                                        // on any path is stored separately, we can do this with confidence because our starting index is greater 
                                                        // than any properties that have already been attached
                                                        Attacher(element, newRef, element.path.replace(start, replaceWith).replace(/\{[^\}]{1,}\}/, pathParam))

                                                    }
                                                    for (let newI = currentSegs.length - 1; newI < newSegments.length; newI++) {
                                                        // start at the end of the current paths segments and go forward attaching
                                                        // e.g /path/to/something and /path/to/something/a/bit/longer the index newI
                                                        // allows us to start at the a(4th) segment and continue attaching properties
                                                        // to path.to.something
                                                        if (!__this._.singleMatchReg.test(newSegments[newI])) {
                                                            if (__this._.singleMatchReg.test(newSegments[newI + 1])) {
                                                                // same explanation as above
                                                                newRef[newSegments[newI]] = genGen(newSegments.slice(0, newI + 1).join('/'), newSegments.slice(0, newI + 1).join('/').replace(start, replaceWith).replace(/\{[^\}]{1,}\}/, pathParam), newI + 1, newSegments, pathsWithParams, ind)
                                                                break;
                                                            } else {
                                                                (newRef[newSegments[newI]] === undefined) ? (newRef[newSegments[newI]] = {}, newRef = newRef[newSegments[newI]]) : (newRef = newRef[newSegments[newI]]);
                                                                if (newI === newSegments.length - 1) {
                                                                    Attacher(element, newRef, element.path.replace(start, replaceWith).replace(/\{[^\}]{1,}\}/, pathParam))
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        });
                                        return returnVal;
                                    }
                                }('', '', j + 1, segments, withParams, i))
                                // keep tracking variable referencing the currently 
                                progress = progress[segments[j]]
                            } else {
                                // function has already been implemented and we skip through
                                // keep tracking variable referencing the currently 
                                progress = progress[segments[j]]
                            }

                            // we just implemented the rest of the 'segments' and attached them to the funciton result so no need to continue the loop
                            break;
                        } else {
                            // we cant get to here at the end of the loop because the path is guaranteed to have in at least on path param
                            // that means this will only be used to fill in pre properties for the path param function
                            ((progress[segments[j]]) === undefined) ? (progress[segments[j]] = {}, progress = progress[segments[j]]) : (progress = progress[segments[j]]);

                        }
                    } else {
                        // this is the path param part which was implemented in the previous iteration
                        // in reality the break statement should skip here
                        attachedToPathParam = true
                        break;
                    }
                }
            }
        }

        if (withoutParams.length > 0) {
            withoutParams.forEach((pathElement) => {
                // none of these properties have path params this process is fairly simple
                var finalRef = attachTo;
                var finalSegs;
                if (typeof pathElement === "string") {
                    finalSegs = pathElement.split('/');
                }
                else {
                    finalSegs = pathElement.path.split('/');
                }
                if (finalSegs[0] === "") {
                    finalSegs.shift();
                }
                finalSegs.forEach((element, fI) => {
                    (finalRef[element] === undefined) ? (finalRef[element] = {}, finalRef = finalRef[element]) : (finalRef = finalRef[element]);
                    if (fI === finalSegs.length - 1) {
                        Attacher(pathElement, finalRef, pathElement.path)
                    }
                })
            })
        }
    }

    // ================================================================================================================================
    //                                  END ENDPOINT CYCLER
    // ================================================================================================================================


    ref(path) {
        return new Instance(this._.hostname, path, true, 10000);
    }

    __handleRes(...params) {
        if (this.readyState == 4 && this.status == 200) {
            console.log(this.status)
            console.log(JSON.parse(this.responseText))
        } else if (this.readyState == 4) {
            console.log(this.status)
            console.log("unsuccessful")

        }
    }

}

/**
 * instance class
 * @class
 */
class Instance {
    private requests: Object;
    private data: Object;
    private host: String;
    private path: String;
    private cache: { cachable: boolean, ttl: number };
    private valid: boolean;
    private infoStore;


    /**
     * constructor
     * @constructor
     * @param {string | Object} host - the _.hostname
     * @param {string} [currentPath] - path to the current resource instance
     * @param {Boolean} [cachable] - is the resource cachable
     * @param {number} [ttl] - time to live if cachable
     */
    constructor(host: String | Object, currentPath?, cachable?, ttl?) {
        if (typeof host === "object") {
            this.infoStore = host
        } else {
            this.cache = { cachable, ttl };
            if (this.cache.cachable) {
                this.valid = true;
                setTimeout(() => {
                    this.valid = false;
                }, this.cache.ttl);
            } else {
                this.valid = false;
            }
            this.path = currentPath;
            this.host = host;
            this.requests = {};
        }

    }

    /**
     * @function
     * @param params 
     * @description generic xhr error handler, just call and bind it to the relevent object
     */
    __handleRes(...params) {
        if (this.readyState === 4 && this.status >= 200 && this.status < 300) {
            console.log(JSON.parse(this.responseText))
            console.log("update successful")
        } else if (this.readyState === 4) {
            console.log(JSON.parse(this.responseText))
            console.log("update unsuccessful")
        }
    }
    set value(val) {
        var _this = this;

        //the relevent data value stored on the data property is also updated
        this.data = val;
        var i = Object.keys(this.requests).length
        this.requests[i] = new XMLHttpRequest()

        //set generic error handler binding to this current xhr object
        this.requests[i].onreadystatechange = this.__handleRes.bind(this.requests[i])
        this.requests[i].open(`POST`, `${_this.host}${_this.path}`, true)
        this.requests[i].setRequestHeader(`Content-Type`, `application/json`)
        this.requests[i].send(JSON.stringify(val))

    }
    get value() {
        if (this.cache.cachable && this.valid && this.data !== undefined) {
            return this.data;
        }
        //simply gets the data in a synchronous manner
        var req = new XMLHttpRequest();
        var url = urlify(this.host, this.path)
        req.open("GET", url, false);
        req.send(null);
        if (req.status == 200) {
        }
        this.data = JSON.parse(req.responseText);
        if (this.cache.cachable && !this.valid) {
            this.valid = true;
            setTimeout(() => {
                this.valid = false;
            }, this.cache.ttl);
        }
        return this.data;

    }

    private run() {
        var __this = this;

        (this.requests === undefined) ? (this.requests = {}) : null;

        var i = Object.keys(this.requests).length - 1

        // incase we stumbled across an already used space (the first will almost always be a clash)
        while (this.requests[++i] !== undefined);
        this.requests[i] = new XMLHttpRequest()

        //set generic error handler binding to this current xhr object
        this.requests[i].onreadystatechange = function () {
            if (this.readyState === 4 && this.status >= 200 && this.status < 300) {
                try {
                    console.log(JSON.parse(this.responseText))
                } catch (e) {
                    console.log((this.responseText))
                }
                console.log("update successful")
                delete __this.requests[i]
            } else if (this.readyState === 4) {
                try {
                    console.log(JSON.parse(this.responseText))
                } catch (e) {
                    console.log((this.responseText))
                }
                console.log("update unsuccessful")
                delete __this.requests[i]
            }
        }
        this.requests[i].open(this.infoStore.method, this.infoStore.url, true)
        if (this.infoStore.headers !== undefined) {
            this.infoStore.headers.forEach((header) => {
                this.requests[i].setRequestHeader(header.name, header.val)

            })
        }
        if (this.infoStore.body !== undefined) {
            this.requests[i].send(JSON.stringify(this.infoStore.body))
        } else {
            this.requests[i].send()
        }
    }

    toString() {

    }
    currentPath: string;
    __: Object;
}

window.WAO = WAO
module.exports = {
    WAO,
    Instance
};