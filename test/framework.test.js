var expect = require("chai").expect
var urlify = require('../framework/helpers').urlify
var tripleParse = require('../framework/helpers').tripleParse

describe("urlify", function () {
    it("should throw error if no arguments are provided", function () {
        expect(urlify.bind(urlify)).to.throw("urlify must be provided with at least one argument")
    })
    describe("should only take in a string as the first argument", function () {

        it("should not accept an array", () => {
            expect(urlify.bind(urlify, [47])).to.throw("urlify's first paramter(hostname) must always be a string")
        })
        it("should not accept a number", () => {
            expect(urlify.bind(urlify, 70)).to.throw("urlify's first paramter(hostname) must always be a string")
        })
        it("should not accept an object", () => {
            expect(urlify.bind(urlify, { a: 32 })).to.throw("urlify's first paramter(hostname) must always be a string")
        })
        it("should not accept an function", () => {
            expect(urlify.bind(urlify, function (params) { })).to.throw("urlify's first paramter(hostname) must always be a string")
        })
        it("should not accept undefined", () => {
            expect(urlify.bind(urlify, undefined)).to.throw("urlify's first paramter(hostname) must always be a string")
        })
        it("should not accept null", () => {
            expect(urlify.bind(urlify, null)).to.throw("urlify's first paramter(hostname) must always be a string")
        })

        it("should not accept a symbol", () => {
            expect(urlify.bind(urlify, Symbol("symbol"))).to.throw("urlify's first paramter(hostname) must always be a string")
        })
        it("should not accept a boolean", () => {
            expect(urlify.bind(urlify, false)).to.throw("urlify's first paramter(hostname) must always be a string")
        })
        it("should accept a string", () => {
            expect(urlify.bind(urlify, "string")).to.not.throw()
        })
    })


    describe("It should only accept strings or numbers for all other parameters", () => {
        it("should not accept an array", () => {
            expect(urlify.bind(urlify, "hostname", [47])).to.throw(TypeError)
            expect(urlify.bind(urlify, "hostname", [47])).to.throw("urilify only accepts strings and numbers to compose the url")
        })
        it("should not accept an object", () => {
            expect(urlify.bind(urlify, "hostname", { a: 32 })).to.throw(TypeError)
            expect(urlify.bind(urlify, "hostname", { a: 32 })).to.throw("urilify only accepts strings and numbers to compose the url")
        })
        it("should not accept an function", () => {
            expect(urlify.bind(urlify, "hostname", function (params) { })).to.throw(TypeError)
            expect(urlify.bind(urlify, "hostname", function (params) { })).to.throw("urilify only accepts strings and numbers to compose the url")
        })
        it("should not accept undefined", () => {
            expect(urlify.bind(urlify, "hostname", undefined)).to.throw(TypeError)
            expect(urlify.bind(urlify, "hostname", undefined)).to.throw("urilify only accepts strings and numbers to compose the url")
        })
        it("should not accept null", () => {
            expect(urlify.bind(urlify, "hostname", null)).to.throw(TypeError)
            expect(urlify.bind(urlify, "hostname", null)).to.throw("urilify only accepts strings and numbers to compose the url")
        })

        it("should not accept a symbol", () => {
            expect(urlify.bind(urlify, "hostname", Symbol("symbol"))).to.throw(TypeError)
            expect(urlify.bind(urlify, "hostname", Symbol("symbol"))).to.throw("urilify only accepts strings and numbers to compose the url")
        })
        it("should not accept a boolean", () => {
            expect(urlify.bind(urlify, "hostname", true)).to.throw(TypeError)
            expect(urlify.bind(urlify, "hostname", false)).to.throw("urilify only accepts strings and numbers to compose the url")
        })
        it("should accept a string", () => {
            expect(urlify.bind(urlify, "hostname", "string")).to.not.throw()
        })
        it("should accept a number", () => {
            expect(urlify.bind(urlify, "hostname", 12)).to.not.throw()
        })
    })
    describe("always return string starting with 'http://' or 'https://'", function () {
        it('("hostname", "test", 1) case', function () {
            expect(urlify("hostname", "test", 1).indexOf("http://") == 0 || urlify("hostname", "test", 1).indexOf("https://") == 0).to.equal(true)
        })
    })
    describe("working with query params", function () {
        it("should accept multiple params statrting with '?'", function () {
            let url = urlify("hostname", "test", 1, "?a=b", "?gee=la","?car=fast", "?house=big")
            expect(url.endsWith("?a=b&gee=la&car=fast&house=big")).to.equal(true)
        })
        it("should accept multiple params statrting with '&'", function () {
            let url = urlify("hostname", "test", 1, "&a=b", "&gee=la", "&car=fast", "&house=big")
            expect(url.endsWith("?a=b&gee=la&car=fast&house=big")).to.equal(true)
        })
        it("should accept multiple params statrting with a mix of '?' and '&'", function () {
            let url = urlify("hostname", "test", 1, "&a=b", "?gee=la", "&car=fast", "?house=big")
            let url2 = urlify("hostname", "test", 1, "?a=b", "&gee=la", "?car=fast", "&house=big")
            expect(url.endsWith("?a=b&gee=la&car=fast&house=big")).to.equal(true)
            expect(url2.endsWith("?a=b&gee=la&car=fast&house=big")).to.equal(true)
            
        })
    })
    describe("should convert to a url encoded string", function () {
        it("", function () {
            expect(false).to.equal(true)
        })
    })
})

describe("tripleParse",function () {
    describe("taking in a string", function () {
        it("should accept a string and parse it", function () {
            let a = tripleParse("mystring", {}, undefined)
            expect(a).to.equal("mystring")
        })
    })
})