# Web As Object

Setup
=====

The setup process is fairly easy for this library simply call the constructor and pass it the json initialization data as well as the spec used e.g

**for package managers**  
If you are using a package manager such as npm you can simple require the constructor like below
```js
var apiInit = require("./init.json")
var WAO = require("/path/to/framework.js").WAO
var myApi = new WAO({ init: apiInit, spec: "oas3.0" })

```
**script tag usage**
for simpler html script tag usage simple add the script for the library as the **First** script in the html document
```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Test a feature</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="path/to/framework.js"></script>
    <script src="path/to/other/script.js"></script>
</head>
...
</html>
```
after this point you can simply use the class constructor
```js
var apiInit = {
    //API Initialaztion data (e.g OAS document)
}
var myApi = new WAO({ init: apiInit, spec: "oas3.0" })

```
In this case Open API Specification 3.0 is used to define the API in the init.json file so 'oas3.0' is specified, this is required.This is practically the end of the setup process, the myApi object can now be used to access the API's endpoints.  
  
Usage
=====
  Take for example an api with a pets endpoint:  
```
url: https://myApi.com/pets  
methods: GET, POST  
```
To access these methods we simply write
```js
myApi.pets.get() //GET method
myApi.pets.post({//POST method
        body: {
            name: "new pet",
            photoUrls: []
        },
        headers: {},    //headers go here
        queryparams: {} //query parameters go here
    })
```

NB: all data is returned in the form of a promise therefore, in order to use this data we must access it via the .then method
```js
myApi.pets.post({
        // query info...
    }).then(function(data){
        //use data here
        console.log(data)
    })
```
This is so for **ALL** functions that access the API.  
  
Query Data
===
Notice the data being passed to the post method  
```js
myApi.pets.get() //GET method
myApi.pets.post({//POST method
        body: {
            name: "new pet",
            photoUrls: []
        },
        headers: {},    //headers go here
        queryparams: {} //query parameters go here
    })
```

- Firstly we have the body property, which is simply the data to be sent as the request body, the body can be in any format accepted by the endpoint
```js
body: [1,2] //array
body: "string" //string
//etc...
```
- The headers property will contain the headers in the format:
```js 
headers:{"headerName": "headerValue"}
```
- And finally the query parameters which take the same format as the headers proeprty
```js 
queryparams:{"parameterName": "parameterValue"}
```

Path Paramters
===
For urls where part of the url is a variable e.g `/pets/{petId}` where **petId** is the ID of the pet we want to access,
accessing pet with ID 20 (`https://myApi.com/pets/20`) will look like this  
```js
myApi.pets(20).get()
```
And likewise, any other http verbs for this endpoint follow suit e,g
```js
myApi.pets(20).delete()
//etc...
```
For those uri's that continue beyond the path parameter, simply carry on as you normally would, to access the owner pf pet 20 at the uri `pets/20/owner` 

```js
myApi.pets(20).owner.get()
```

Http Verbs
===

Notice how every api call ends with an http verb this is true for all endpoints in any api, failing to do so will return a refernce to that current point
```js
var ownerRef = myApi.pets(20).owner
// The above code will return a reference to directly access the owner for pet #20 on the API

ownerRef.get() // This is equivalent to myApi.pets(20).owner.get()
```